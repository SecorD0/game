#!/bin/bash
# Options
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/colors.sh) --
option_value(){ echo "$1" | sed -e 's%^--[^=]*=%%g; s%^-[^=]*=%%g'; }
while test $# -gt 0; do
	case "$1" in
	-h|--help)
		. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
		echo
		echo -e "${C_LGn}Functionality${RES}: the script installs a Game node"
		echo
		echo -e "${C_LGn}Usage${RES}: script ${C_LGn}[OPTIONS]${RES}"
		echo
		echo -e "${C_LGn}Options${RES}:"
		echo -e "  -h, --help  show the help page"
		echo
		echo -e "${C_LGn}Useful URLs${RES}:"
		echo -e "https://t.me/letskynode — node Community"
		echo
		return 0 2>/dev/null; exit 0
		;;
	*|--)
		break
		;;
	esac
done
# Functions
printf_n(){ printf "$1\n" "${@:2}"; }
# Actions
sudo apt install wget -y &>/dev/null
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
sudo apt update
sudo apt upgrade -y
sudo apt install git make jq pkg-config build-essential libssl-dev -y
cd nibiru
git fetch --all --tags
git checkout -b neuron-1.1 tags/neuron-1.1
make install
cd
nibirud version
wget -qO $HOME/.nibiru/config/genesis.json https://raw.githubusercontent.com/cosmos-gaminghub/testnets/master/neuron-1/genesis.json
nibirud unsafe-reset-all
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/miscellaneous/ports_opening.sh) 26656 26657
sed -i -e "s%^laddr *=.*%laddr = \"tcp://`wget -qO- eth0.me`:26657\"%; s%^moniker *=.*%moniker = \"$game_moniker\"%; s%^seeds *=.*%seeds = \"ac175b66221b555751b3a5fb2e6a8844ba01228d@167.179.104.210:26656,649e13457ce5e8c5d7cd7845625a6f8b4ded9eb9@62.171.177.10:26656,15ce53fe92b159b711878d4256abf4f8156751f2@135.181.26.211:26656\"%; " $HOME/.nibiru/config/config.toml
sed -i "s%minimum-gas-prices *=.*%minimum-gas-prices = \"0.0001ugame\"%" $HOME/.nibiru/config/app.toml
printf "[Unit]
Description=Game Node
After=network-online.target

[Service]
User=$USER
ExecStart=$(which nibirud) start
Restart=on-failure
RestartSec=3
LimitNOFILE=65535

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/gamed.service
sudo systemctl daemon-reload
sudo systemctl enable gamed
sudo systemctl restart gamed
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/miscellaneous/insert_variable.sh) -n game_log -v "sudo journalctl -f -n 100 -u gamed" -a
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/miscellaneous/insert_variable.sh) -n game_node_info -v ". <(wget -qO- https://gitlab.com/SecorD0/game/-/raw/main/node_info.sh) -l RU 2> /dev/null" -a
printf_n "${C_LGn}Done!${RES}"